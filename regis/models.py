from django.db import models


class pengguna(models.Model):
    username = models.CharField(max_length=20 , primary_key=True)
    password = models.CharField(max_length=20)
    role = models.CharField(max_length=20)
    class Meta:
        db_table = 'simbion.pengguna'

class donatur(models.Model):
    nomor_identitas = models.CharField(max_length=20, primary_key=True)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=20)
    npwp = models.CharField(max_length=20)
    no_telp = models.CharField(max_length=20)
    alamat = models.CharField(max_length=50)
    username = models.ForeignKey(pengguna ,on_delete= models.CASCADE, db_column='username')
    
    class Meta:
        db_table = 'simbion.donatur'

class admin(models.Model):
    username = models.ForeignKey(pengguna,on_delete= models.CASCADE, db_column='username')
    class Meta:
        db_table = 'simbion.admin'

class individual_donor(models.Model):
    nik = models.CharField(max_length=16)
    nomor_identitas_donatur = models.ForeignKey(donatur ,on_delete= models.CASCADE, db_column='nomor_identitas_donatur')
    class Meta:
        db_table = 'simbion.individual_donor'


class yayasan(models.Model):
    no_sk_yayasan = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp_cp = models.CharField(max_length=20)
    nomor_identitas_donatur = models.ForeignKey(donatur ,on_delete= models.CASCADE,db_column='nomor_identitas_donatur')
    class Meta:
        db_table = 'simbion.yayasan'



class mahasiswa(models.Model) :
    npm = models.CharField(max_length=20 , primary_key=True)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp = models.CharField(max_length=20)
    alamat_tinggal = models.CharField(max_length=50)
    alamat_domisili = models.CharField(max_length=50)
    nama_bank = models.CharField(max_length=50)
    no_rekening = models.CharField(max_length=20)
    nama_pemilik = models.CharField(max_length=20)
    username = models.ForeignKey(pengguna ,on_delete= models.CASCADE, db_column='username')
    class Meta:
        db_table = 'simbion.mahasiswa'

# Create your models here.
