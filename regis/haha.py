from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
import os
import json

response = {}

def index (request):
    return render (request, 'register.html', response)

def regMahasiswa(request):
    return render (request, 'registerMahasiswa.html', response)

def regDonIndividu(request):
    return render (request, 'registerDonIndividu.html', response)

def regYayasan(request):
    return render (request, 'registerYayasan.html', response)

@csrf_exempt
def add_mahasiswa (request):
    if request.method == 'POST':
        cursor = connection.cursor()

        username = request.POST['username']
        password = request.POST['password']
        npm = request.POST['npm']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        alamat_tinggal = request.POST['alamat_tinggal']
        alamat_domisili = request.POST['alamat_domisili']
        nama_bank = request.POST['nama_bank']
        no_rekening = request.POST['no_rekening']
        nama_pemilik = request.POST['nama_pemilik']

        cursor.execute('SELECT username FROM pengguna')
        daftarPengguna =  cursor.fetchall()
        cursor.execute('SELECT npm FROM mahasiswa')
        daftarMahasiswa = cursor.fetchall()

        f_username = False
        f_npm = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                f_username = True
        for nomor in daftarMahasiswa :
            if (nomor[0] == npm):
                f_npm = True



        f_angka = False
        f_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                f_angka = True
        if len(password)>7 :
            f_panjang = True

        f_password = f_panjang and f_angka


        if (not f_username) and f_password :
            
            query = "INSERT INTO pengguna VALUES ('%s', '%s', 'mahasiswa')" %\
            (username, password)
            cursor.execute(query)

        if (not f_npm):
            query = "INSERT INTO mahasiswa VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username)
            cursor.execute(query)

        cursor.close()

        return HttpResponseRedirect(reverse('login:index'))

def select_donatur_type(request):
    if request.method == 'POST':
        if request.POST['selected_donatur_type'] == 'individual':
            response['donatur_type'] = request.POST['selected_donatur_type']
            return render (request, 'register/register_donatur.html', response)
        elif request.POST['selected_donatur_type'] == 'yayasan':
            response['donatur_type'] = request.POST['selected_donatur_type']
            return render (request, 'register/register_donatur.html', response)

@csrf_exempt
def add_donatur_individual(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nik = request.POST['nik']
        email = request.POST['email']
        nama = request.POST['nama']
        npwp = request.POST['npwp']
        no_telp = request.POST['no_telp']
        alamat = request.POST['alamat']

        cursor = connection.cursor()

        #CEK USERNAME BELUM TERDAFTAR
        cursor.execute('SELECT username FROM pengguna')
        daftarPengguna = cursor.fetchall()

        f_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                f_username = True

        cursor.execute('SELECT nomor_identitas FROM donatur')
        daftarNomorIdentitas = cursor.fetchall()
        f_nomor_identitas = False

        for nomor in daftarNomorIdentitas :
            if (nomor[0] == nomor_identitas):
                f_nomor_identitas = True

        #Check that password have minimal eight character and any digit there
        f_angka = False
        f_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                f_angka = True
        if len(password)>7 :
            f_panjang = True

        f_password = f_panjang and f_angka

        daftarNIK = cursor.fetchall()
        
        for nomor in daftarNIK :
            if (nomor[0] == nik):
                f_nik = True
            if len(nik)==16:
                f_nik_panjang = True

        cursor.execute('SELECT nik FROM individual_donor;')

        if (not f_username) and f_password and (not f_nomor_identitas) and (not f_nik) and f_nik_panjang :
            query = "INSERT INTO pengguna VALUES ('%s', '%s', 'donatur')" %\
            (username, password)
            cursor.execute(query)
            query2 = "INSERT INTO donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
            query3 = "INSERT INTO individual_donor VALUES ('%s', '%s')" %\
            (nik, nomor_identitas)
            cursor.execute(query2)
            cursor.execute(query3)
            cursor.close()


@csrf_exempt
def add_donatur_yayasan (request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nomor_sk_yayasan = request.POST['nomor_sk_yayasan']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        npwp = request.POST['npwp']
        alamat = request.POST['alamat']

        cursor = connection.cursor()
        cursor.execute('SELECT username FROM pengguna')
        daftarPengguna = cursor.fetchall()

        f_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                f_username = True

        cursor.execute('SELECT nomor_identitas FROM donatur')
        daftarNomorIdentitas = cursor.fetchall()
        f_nomor_identitas = False

        for nomor in daftarNomorIdentitas :
            if (nomor[0] == nomor_identitas):
                f_nomor_identitas = True

        f_angka = False
        f_panjang = False

        for huruf in password :
            if (huruf.isdigit()):
                f_angka = True
        if len(password)>7 :
            f_panjang = True

        f_password = f_panjang and f_angka

        cursor.execute('SELECT no_sk_yayasan FROM yayasan;')
        daftar_nomor_SK_yayasan = cursor.fetchall()


        for nomor in daftar_nomor_SK_yayasan :
            if (nomor[0] == daftar_nomor_SK_yayasan):
                f_SK = True

        if (not f_username) and f_password and (not f_nomor_identitas) and (not f_SK) :
            query = "INSERT INTO pengguna VALUES ('%s', '%s', 'donatur');" %\
            (username, password)
            cursor.execute(query)
            query2 = "INSERT INTO donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s');" %\
            (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
            query3 = "INSERT INTO yayasan VALUES ('%s', '%s', '%s', '%s', '%s');" %\
            (nomor_sk_yayasan, email, nama, no_telp, nomor_identitas)
            cursor.execute(query2)
            cursor.execute(query3)
            cursor.close()

        return HttpResponseRedirect(reverse('login:index'))