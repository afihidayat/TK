from django.conf.urls import url
from .views import index, donatur_yayasan, donatur_individual, regis_mahasiswa, role_donatur


#url for app
app_name = 'regis'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^donatur-yayasan', donatur_yayasan, name='donatur_yayasan'),
    url(r'^donatur-individual', donatur_individual, name='donatur_individual'),
    url(r'^mahasiswa',regis_mahasiswa, name='regis_mahasiswa'),
    url(r'^role-donatur',role_donatur, name='role_donatur'),
]